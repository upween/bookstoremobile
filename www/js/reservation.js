function fillboard(funct,control) {
    var path =  serverpath + "boardmaster/0/1"
    ajaxget(path,'Filldataboard','comment','');
  }
  
  function Filldataboard(data,control){  
    data = JSON.parse(data);
    data = data.recordset;
    jQuery("#board").empty();
    jQuery("#board").append(jQuery("<option selected disabled></option>").val("0").html("Select Board"));
    for (var i = 0; i < data.length; i++) {
        jQuery("#board").append(jQuery("<option ></option>").val(data[i].BoardID).html(data[i].BoardName));
    }          
}
$("#board").change(function () {    
    fillschool('',$("#board").val()); 
    
}); 
function fillschool(funct,control) {
    var path =  serverpath + "SchoolMaster/0/"+control
    ajaxget(path,'Filldataschool','comment','');
  }
  
  function Filldataschool(data,control){  
    data = JSON.parse(data);
    data = data.recordset;
    jQuery("#school").empty();
    jQuery("#school").append(jQuery("<option selected disabled></option>").val("0").html("Select School"));
    for (var i = 0; i < data.length; i++) {
        jQuery("#school").append(jQuery("<option></option>").val(data[i].SchoolID).html(data[i].SchoolName));
    }
    fillclass('',$("#board").val());          
}
function fillclass(funct,control) {
    var path =  serverpath + "BoardClass/"+control+"/1";
    ajaxget(path,'Filldataclass','comment','');
  }
  
  function Filldataclass(data,control){  
    data = JSON.parse(data);
    data = data.recordset;
    jQuery("#class").empty();
    jQuery("#class").append(jQuery("<option selected disabled></option>").val("0").html("Select Class"));
    for (var i = 0; i < data.length; i++) {
        jQuery("#class").append(jQuery("<option></option>").val(data[i].ClassID).html(data[i].ClassName));
    }          
}
function checkValidiation(){
    if($.trim($('#name').val())==''){
        toastr.warning('Please Enter Your Name');
        return false;
    }
    if($.trim($('#mobile').val())==''){
        toastr.warning('Please Enter Your Mobile Number');
        return false;
    }
    if($('#board').val()=='0'){
        toastr.warning('Please Select Board');
        return false;
    }
    if($('#school').val()=='0'){
        toastr.warning('Please Select School');
        return false;
    }
    if($('#class').val()=='0'){
        toastr.warning('Please Select Class');
        return false;
    }
    else{
        insUpdEnquiry();
    }
}
function insUpdEnquiry(){
    var MasterData = {
    "UniqueID":'0',
    "BaordID":$('#board').val(),
    "SchoolID":$('#school').val(),
    "ClassID":$('#class').val(),
    "Name":$('#name').val(),
    "MobileNumber":$('#mobile').val()
    };
MasterData = JSON.stringify(MasterData)
    var path = serverpath + "bookaset";
    ajaxpost(path, 'bookASet', 'comment', MasterData, 'control')
}
function bookASet(data){
    data = JSON.parse(data)
    var data1=data.recordset[0]
    if (data1.ReturnValue == "1") {
        toastr.success('Enquiry sent successfully');
        window.location = "index.html";
        return true;
    }
}