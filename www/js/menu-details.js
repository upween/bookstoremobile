function productdetails(productid){
    var path = serverpath + "productdetailsbyid/"+productid+"/1"
    ajaxget(path,'fillproductdetails','comment','control');
}
function fillproductdetails(data,control){
    data = JSON.parse(data);
    data = data.recordset;
    $("#tab1image").attr("src", photopath+data[0].Image1);
    $("#tab1small").attr("src", photopath+data[0].Image1);
    if (data[0].Image2 != "undefined"){
        $("#tab2image").attr("src", photopath+data[0].Image2);
        $("#tab2small2").show();
        $("#tab2small2").attr("src", photopath+data[0].Image1);
    } 
    else{
        $("#tab2small2").hide();
        $("#tab2small2").prop( "disabled", true );
    }
    if (data[0].Image3 != "undefined"){
        $("#tab3image").attr("src", photopath+data[0].Image3);
        $("#tab3small3").show();
        $("#tab3small3").attr("src", photopath+data[0].Image1);
    }
    else{
        $("#tab3small3").hide();
        $("#tab3small3").prop( "disabled", true );
    }
    $("#boardname").html(data[0].BoardName);
    $("#classname").html(data[0].ClassName);
    $("#productname").html(data[0].ProductName);
    $("#price").html(data[0].Price + " ₹");
    $("#description").html(data[0].ProductDescription);
    $("#authorname").html(data[0].AuthorName);
    $("#publisher").html(data[0].Publisher);
    $("#imprint").html(data[0].Imprint);
    $("#language").html(data[0].Language);
}

$("#addtocart").click(function(){
    insUpdCard();
});
function insUpdCard(){
    var MasterData = {
        "UniqueID":'0',
        "UserID":Cookies.get('permUserId')?Cookies.get('permUserId'):Cookies.get('tempUser'),
        "Product_ID":sessionStorage.ProductId,
        "Quantity":1,
        "EntryFrom":'',
        "Price":$('#price').text().replace(' ₹',''),
        "DiscountedPrice":'',  
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "usercart";
    ajaxpost(path, 'addToCart', 'comment', MasterData, 'control')
}
function addToCart(data){
    data = JSON.parse(data)
    var data1=data.recordset[0]
    if (data1.ReturnValue == "1") {
        if(!Cookies.get('cartcount')){
            Cookies.set('cartcount', "1",{ expires: 365, path: '/' });  
        }
        else{
            var ct = Cookies.get('cartcount')
            ct = parseInt(ct)+1;
            Cookies.set('cartcount', ct ,{ expires: 365, path: '/' });  
        }
        $("#cartvalue").attr("data-count",Cookies.get('cartcount'));
    }
}
  

