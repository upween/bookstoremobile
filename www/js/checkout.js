function checkValidiation(){
    if($.trim($('#name').val())==''){
        toastr.warning('Please Enter Your Name');
        return false;
    }
    else if($.trim($('#email').val())==''){
        toastr.warning('Please Enter Email');
        return false;
    }
    else if($.trim($('#mobile').val())==''){
        toastr.warning('Please Enter Your Mobile Number');
        return false;
    }
    else if($('#address').val()==''){
        toastr.warning('Please Enter Your Shipping Address');
        return false;
    }
    else if($('#city').val()=='0'){
        toastr.warning('Please Select City');
        return false;
    }
    else if($('#pincode').val()==''){
        toastr.warning('Please Enter Your PINCODE');
        return false;
    }
    else{
        $('#content').html("SMS will be sent to you Mobile Number " +$('#mobile').val() + " Do you wish to continue")
        $('#ex1').modal();
    }
}
function continuefirst(){
    SendOTP("Verify");
}
function closemodal(){
    $('#first').show();
    $('#second').hide();
    $.modal.close();
}
function continuesecond(){
    if ($("#sms").val() == ""){
        toastr.warning('Please Enter OTP');
        return false;
    }
    else{
        SendOTP("Match");
    }  
}
function SendOTP(flag){
    var otp='';
    if(flag=='Verify'){
       otp =generateOTP();
       //otp ="1234";
       sessionStorage.setItem("OTP",otp)
    }
    if(flag=='Match'){
        otp=$('#sms').val();
    }
    var MasterData = {
        "MobileNumber":$('#mobile').val(),
        "Otp":otp,
        "Flag":flag       
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "MobileVerification";
    ajaxpost(path, 'mobileVerification', 'comment', MasterData, 'control');
}
function mobileVerification(data){
    data = JSON.parse(data)
    var data1=data.recordset[0]
    if (data1.ReturnValue == "1") {
        $('#first').hide();
        $('#second').show();
        sendmsg("Your login OTP is "+sessionStorage.getItem("OTP"),$('#mobile').val());
        return true;
    }
    else if (data1.ReturnValue == "2") {
        insupdnewUserDetail();
        return true;      
    }
    else if(data1.ReturnValue=="3"){
        $('#sms').val("");
        toastr.warning('Please Enter Correct OTP');
        return false;
    }
}
function insupdnewUserDetail(){
    var MasterData = {
        "UserDetailID":'0',
        "FirstName":$('#name').val(),
        "LastName":'',
        "EmailAddress":$('#email').val(),
        "Address":$('#address').val(),
        "StreetAddress2":'0',
        "City":'1',
        "State":'0',
        "PostalCode":$('#pincode').val(),
        "Country":"0",
        "Telephone":$('#mobile').val(),
        "TempUserID":Cookies.get('tempUser')
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "user";
    ajaxpost(path, 'newuserDetail', 'comment', MasterData, 'control')
}
function newuserDetail(data){
    data = JSON.parse(data)
    var data1=data.recordset[0]
    if (data1.ReturnValue == "1" || data1.ReturnValue=='2' ) {
        Cookies.set('permUserId', data1.UserDetailID,{ expires: 3600 * 1000 * 24 * 365, path: '/' });
        insUpdAndroid();
    }
 }
 function insUpdAndroid(){
    var MasterData = {
        "UserID":Cookies.get('permUserId'),
        "RegistrationID":Cookies.get('permUserId'),
        "DeviceUUID":sessionStorage.getItem("uuid"),
        "DeviceModel":sessionStorage.getItem("model"),
        "DevicePlatform":sessionStorage.getItem("platform"),
        "DeviceCordova":sessionStorage.getItem("cordova"),
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "AndroidReg";
    ajaxpost(path, 'newAndroidReg', 'comment', MasterData, 'control')
 }
 function newAndroidReg(data){
    data = JSON.parse(data)
    var data1=data.recordset[0]
    if (data1.ReturnValue == "1" || data1.ReturnValue=='2' ) {
        insUpdOrder();
    }
 }
 function insUpdOrder(){
    var MasterData = {        
        "OrderID":'0',
        "UserID":Cookies.get('permUserId'),
        "Subtotal": $('#subtotal').text().replace(' ₹',''),
        "ShippingCharge":$('#shippingfee').text().replace(' ₹',''),
        "TotalAmount":$('#grandtotal').text().replace(' ₹',''),
        "PaymentMode": $('input[name="payment-chossed"]:checked').val(),
        "Address":$('#address').val(),
        "OrderDetails":[],
        "Status":'Pending'
    };
    var details = sessionStorage.OrderDetails;
    details=JSON.parse(details).recordset;
    for(var i = 0; i < details.length; i++){
        var detail = details[i];
        var orderdetail= {
            "PRODUCT_ID":detail.Product_ID,
            "QUANTITY":detail.Quantity
        }
        MasterData.OrderDetails.push(orderdetail);
    }
    MasterData = JSON.stringify(MasterData);
    var path = serverpath + "Orders";
    ajaxpost(path, 'ordersCreate', 'comment', MasterData, 'control');       
}
function ordersCreate(data){
    data=JSON.parse(data);
    data1=data.recordset[0];
    if(data1.ReturnValue=='1'){
        sessionStorage.OrderID=data1.OrderID;
        window.location='done-process.html';
    }
}