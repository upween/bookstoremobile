//var serverpath="http://localhost:3203/"
var serverpath="http://182.70.254.93:303/"
var photopath = "http://182.70.254.93:300/product/";

function securedajaxget(path, type, comment, control) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        headers: { 'authorization': sessionStorage.getItem("token"), 'refreshToken': sessionStorage.getItem("refreshToken") },
        url: path,
        cache: false,
        dataType: "json",
        success: function (successdata) {
            window[type](JSON.stringify(successdata), control);
        },
        error: function (errordata) {
            if (errordata.status == 0) {
                toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                return true;
            }
            else if (errordata.status == 401) {
                window[type](JSON.stringify(errordata.responseJSON), control);
                return true;
            }
            else {
                window[type](JSON.stringify(errordata), control);
                return true;
            }
        }
    });
}
function securedajaxpost(path, type, comment, masterdata, control) {
    jQuery.ajax({
        url: path,
        type: "POST",
        headers: { 'authorization': sessionStorage.getItem("token"), 'refreshToken': sessionStorage.getItem("refreshToken") },
        data: masterdata,
        contentType: "application/json; charset=utf-8",
        success: function (successdata, status, jqXHR) {
            window[type](JSON.stringify(successdata));
        },
        error: function (errordata) {
            if (errordata.status == 0) {
                toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                return true;
            }
            else if (errordata.status == 401) {
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
            else {
                window[type](JSON.stringify(errordata));
            }
        }
    });
}
function ajaxget(path, type, comment,control) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: path,
        cache: false,
        dataType: "json",
        success: function (successdata) {
            window[type](JSON.stringify(successdata));
        },
        error: function (errordata) {
            if (errordata.status == 0) {
                toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                return true;
            }
            else {
                window[type](JSON.stringify(errordata));
            }
        }
    });
}
function ajaxpost(path, type, comment, masterdata, control) {
    jQuery.ajax({
        url: path,
        type: "POST",
        data: masterdata,
        contentType: "application/json; charset=utf-8",
        success: function (successdata, status, jqXHR) {
            window[type](JSON.stringify(successdata));
        },
        error: function (errordata) {
            if (errordata.status == 0) {
                toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                return true;
            }
            else {
                window[type](JSON.stringify(errordata));
            }
        }
    });
}


function getvalidated(controlid, type, format) {
    var validid = "valid" + controlid;
    if (type == "text") {
        if ($.trim($("#" + controlid).val()) == "") {
            $("#" + controlid).css('border-color', 'red');
            $("#" + validid).html("Please Enter " + format);
        }
        else {
            jQuery("#" + controlid).css('border-color', '');
            $("#" + validid).html("");
        }
    }
    else if (type == "select") {
        if ($("#" + controlid).val() == "0" || $("#" + controlid).val() == "99999") {
            $("#" + controlid).css('border-color', 'red');
            $("#" + validid).html("Please Select " + format);
        }
        else if ($("#" + controlid).val() == "") {
            $("#" + controlid).css('border-color', 'red');
            $("#" + validid).html("Please Select " + format);
        }
        else if ($("#" + controlid).val() == null) {
            $("#" + controlid).css('border-color', 'red');
            $("#" + validid).html("Please Select " + format);
        }
        else {
            jQuery("#" + controlid).css('border-color', '');
            $("#" + validid).html("");
        }
    }
    else if (type == "email") {
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if ($.trim($("#" + controlid).val()) == "") {
            $("#" + controlid).css('border-color', 'red');
            $("#" + validid).html("Please Enter " + format);
        }
        else if ($("#" + controlid).val() != "") {
            if ($("#" + controlid).val().match(mailformat)) {
                jQuery("#" + controlid).css('border-color', '');
                $("#" + validid).html("");
                return true;
            }
            else {
                $("#" + controlid).val("");
                $("#" + controlid).css('border-color', 'red');
                $("#" + validid).html("Please Enter Correct " + format);
                return false;
            }
        }
        else {
            jQuery("#" + controlid).css('border-color', '');
            $("#" + validid).html("")
            return true;
        }
    }
    else if (type == "number") {
        if (format == "Mobile Number") {
            if ($.trim($("#" + controlid).val()) == "") {
                $("#" + controlid).css('border-color', 'red');
                $("#" + validid).html("Please Enter " + format);
            }
            else if ($("#" + controlid).val() != "") {
                var phoneno = /^\d{10}$/;
                if ($("#" + controlid).val().match(phoneno)) {
                    jQuery("#" + controlid).css('border-color', '');
                    $("#" + validid).html("");
                    return true;
                }
                else {
                    $("#" + controlid).css('border-color', 'red');
                    $("#" + validid).html("Please Enter Correct " + format);
                    return false;
                }
            }
            else {
                jQuery("#" + controlid).css('border-color', '');
                $("#" + validid).html("");
            }
        }
    }

}
function onlyNumbers(event) {
    if (event.type == "paste") {
        var clipboardData = event.clipboardData || window.clipboardData;
        var pastedData = clipboardData.getData('Text');
        if (isNaN(pastedData)) {
            event.preventDefault();

        } else {
            return;
        }
    }
    var keyCode = event.keyCode || event.which;
    if (keyCode == 9){
        return true;
    }
    if ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105)) { 
        keyCode -= 48;
    }
    if ((keyCode >= 48 && keyCode <= 57) ) { 
        return true;
    }
    var charValue = String.fromCharCode(keyCode);
    if (isNaN(parseInt(charValue)) && event.keyCode != 8) {
        event.preventDefault();
    }
}



function checkIfFileLoaded(fileName) {
    $.get(fileName, function (data, textStatus) {
        if (textStatus == "success") {
            globalfilestatus = true;
        }
    });
}
function onLoad() {
    document.addEventListener("deviceready", onDeviceReady, false);
}

function onDeviceReady() {
    document.addEventListener("pause", onPause, false);
    document.addEventListener("resume", onResume, false);
    document.addEventListener("backbutton", onBackKeyDown, false);
    document.addEventListener("offline", onOffline, false);
}
function onPause() {
    window.location = "index.html"
}
function onResume() {
    window.location = "index.html"
}
function onOffline() {
    toastr.error("You are offline", "", "error")
}
function onBackKeyDown(){
    var pageURL = $(location).attr("href");
    var newURL = pageURL.split('/').pop();
    if (newURL == "index.html"){
        navigator.app.exitApp();
    }
    else if (newURL == "done-process.html"){
        window.location = "index.html"
    }
    else{
        window.history.back();
    }
}