function paintmenu(){
    var data=sessionStorage.getItem("MenuItems");
    var data1=sessionStorage.getItem("MenuItems");
    var i = 1;
    var iconclass="";
    jQuery("#slide-out").append('<li><div class="user-view"><h2><span>P</span>ustakein</h2></div></li>')
    jQuery.each(jQuery.parseJSON(data), function (key, value) {
        if (value.ParentID == '0') {
            if (value.LinkURL == "/") {
                jQuery("#slide-out").append('<li><a href=index.html><i class="fa fa-home"></i>' + value.MenuName + '</a></li>');
            }
            else if (value.LinkURL == "#") {
                if (i==2) {iconclass = encodeURI("fa fa-list")}
                else if (i==3) {iconclass = encodeURI("fa fa-file")}
                else if (i==4) {iconclass = encodeURI("fa fa-tags")}
                else if (i==5) {iconclass = encodeURI("fa fa-book")}
                else if (i==6) {iconclass = encodeURI("fa fa-rss")}
                else if (i==7) {iconclass = encodeURI("fa fa-clock-o")}
                jQuery("#slide-out").append('<li><div class="collapsible-header"><i class="'+decodeURI(iconclass)+'"></i>' + value.MenuName + '<span><i class="fa fa-caret-right right"></i></span></div><div class="collapsible-body"><ul id=MenuHeader' + i + '></ul></div></li>');
            }
            jQuery.each(jQuery.parseJSON(data1), function (key, value1) {
                if (value.MenuID == value1.ParentID)
                    jQuery("#MenuHeader" + i).append('<li><a onclick="redirecttoproductpage('+value.MasterID+','+value1.MasterID+')">' + value1.MenuName + '</a></li>');
            });
            i++;
        }
    });
    jQuery("#slide-out").append('<li><a href=contact.html><i class="fa fa-home"></i>Contact Us</a></li>');
    jQuery("#slide-out").append('<li><a href=about.html><i class="fa fa-home"></i>About Us</a></li>');
    jQuery("#navbar").append('<div class="container"><div class="row"><div class="col s3"><div class="content-left"><a href="#slide-out" data-target="slide-out" class="sidenav-trigger"><i class="fa fa-bars"></i></a></div></div><div class="col s6"><div class="content-center"><a href="index.html"><h1>Pustakein</h1></a></div></div><div class="col s3"><div class="content-right"><a onclick=gotocart()><i id="cartvalue" data-count="" class="fa fa-shopping-cart badge"></i></a></div></div></div></div>');
    jQuery("#navbarbottom").append('<div class="container"><div class="row"><div class="col s9"><div class="content-center"><a href="reservation.html"><h1>Complete Book Set</h1></a></div></div><div class="col s3"><div class="content-right"><a href="reservation.html"><i class="fa fa-clipboard"></i></a></div></div></div></div>');
    $("#cartvalue").attr("data-count",Cookies.get('cartcount'));
}

function gotocart(){
    window.location = "cart.html";
}
function redirecttoproductpage(boardid,classid){
    sessionStorage.setItem("BoardId",boardid);
    sessionStorage.setItem("ClassId",classid);
    sessionStorage.setItem("Type","Class");
    window.location = "category-details.html";
}
toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": true,
    "progressBar": false,
    "positionClass": "toast-bottom-center",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};
function generateOTP() { 
    var digits = '0123456789'; 
    let OTP = ''; 
    for (let i = 0; i < 4; i++ ) { 
        OTP += digits[Math.floor(Math.random() * 10)]; 
    } 
    return OTP; 
}
function sendmsg(Msg,Mobile){
    var parameter={
        "sender": "PUSTOK",
        "route": "4",
        "country": "91",
        "sms": [
        {
            "message": Msg,
            "to": [Mobile] 
        },    ]
    }
    parameter=JSON.stringify(parameter);
    var settings = {
        "async": true,
        "crossDomain": true,
        "url":serverpath +'sendsms?'+parameter+'',
        "method": "GET",
        "headers": {
            "content-type": "application",
            "Access-Control-Allow-Origin": "*"
        },
    }
    $.ajax(settings).done(function (response) {
        console.log(response);
    });
}