function bindproducts(){
    
    if (sessionStorage.getItem("Type") == "Board"){
        var path = serverpath + "boardwiseproduct/"+sessionStorage.getItem("BoardId")+"/1"
        ajaxget(path,'fillproducts','comment','control');
    }
    else if (sessionStorage.getItem("Type") == "Class"){
        var path = serverpath + "boardclasswiseproduct/"+sessionStorage.getItem("BoardId")+"/"+sessionStorage.getItem("ClassId")+"/1"
        ajaxget(path,'fillproducts','comment','control');
    }
    
}
function fillproducts(data,control){
    data = JSON.parse(data);
    data = data.recordset;
    var i = 1;
    if (data.length == 0){
        $("#productwise").html("No Books Available");
    }
    else{
        if (sessionStorage.getItem("Type") == "Board"){
            $("#productwise").html(data[0].BoardName);
        }
        else if (sessionStorage.getItem("Type") == "Class"){
            $("#productwise").html(data[0].BoardName + " - " + data[0].ClassName);
        }
    }
    
    
    jQuery.each(data, function (key, value) {
        var j=0;
        j=i-1;
        var id = "container"+j;
        var productname = value.ProductName;
        if ( i % 2 == 0) {
            jQuery("#"+id).append('<div class="col s6"><a onclick="redirectpage('+value.ProductID+')"><div class="content"><img src='+photopath+value.Image1+' alt="menu"><div class="text"><h6 class="wraptext">'+value.ProductName+'</h6><p class="price">'+value.Price+' ₹</p></div></div></a></div>')
        }else{
            jQuery("#container").append('<div class="row" id=container'+i+'><div class="col s6"><a onclick="redirectpage('+value.ProductID+')"><div class="content"><img src='+photopath+value.Image1+' alt="menu"><div class="text"><h6 class="wraptext">'+value.ProductName+'</h6><p class="price">'+value.Price+' ₹</p></div></div></a></div></div>');
        }
        i++;
    });
}
function redirectpage(productid){
    sessionStorage.setItem("ProductId",productid);
    window.location = "menu-details.html";
}