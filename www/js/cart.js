function fetchUserCart() {
    var userId=Cookies.get('permUserId')?Cookies.get('permUserId'):Cookies.get('tempUser');
    //var userId = sessionStorage.getItem("tempUser");
    var path = serverpath + "usercart/"+userId+"";
    ajaxget(path,'fillCartItems','comment','control');
};
function fillCartItems(data,control){ 
    sessionStorage.OrderDetails=data;
    data=JSON.parse(data);
    data1=data.recordset;
    var appenddata='';
    var subtotal=0;
    $('#productcontent').empty();
    if(data.recordset.length){
        var subtotal=0;
        for(var i=0 ; i < data1.length ; i++){
            var producttotal=0;
            producttotal=parseInt(data1[i].Price.replace('?',''))*parseInt(data1[i].Quantity);
            appenddata +=`<div class="content"><div class="row"><div class="col s4"><div class="wrap-image">
            <img style="height: 75px;" src="`+photopath+data1[i].Image1+`" alt="">
            </div></div><div class="col s7"><div class="wrap-name"><a href="#">"`+data1[i].ProductName+`"</a></div></div><div class="col s1">
            <div class="icon-remove"><a href="#" onclick= deleteCartItem(`+data1[i].UniqueID+`)><i class="fa fa-remove"></i></a></div></div>
            </div>
            <div class="row"><div class="col s4"><div class="wrap-price"><p>Price</p></div></div>
            <div class="col s8"><div class="wrap-price"><p class="price" id=price`+i+`>`+data1[i].Price+` ₹</p></div>
            </div></div>
            <div class="row"><div class="col s4"><div class="wrap-quality"><p>Quality</p></div>
            </div><div class="col s4"><div class="wrap-quality"><div class="def-number-input number-input safari_only">
            <button onclick="this.parentNode.querySelector('input[type=number]').stepDown();  minuscounter(`+i+`,`+data1[i].Price+`,`+data1[i].UniqueID+`);" class="minus">
            </button><input class="quantity" id=qty`+i+` readonly min="1" name="quantity" value=`+data1[i].Quantity+` type="number">
            <button onclick="this.parentNode.querySelector('input[type=number]').stepUp(); pluscounter(`+i+`,`+data1[i].Price+`,`+data1[i].UniqueID+`);" class="plus"></button></div>
            </div></div></div>
            
            <div class="row"><div class="col s4"><div class="wrap-price"><p>Total Price</p></div></div>
            <div class="col s8"><div class="wrap-price"><p class="price prototal" id=prototal`+i+`>`+producttotal+` ₹</p></div>
            </div></div></div>`
            subtotal = parseInt(subtotal)+parseInt(producttotal);
        }
        $('#productcontent').append(appenddata);
        $('#subtotal').html(subtotal+" ₹");
        $('#shippingfee').html("50 ₹");
        $('#grandtotal').html((parseInt(subtotal)+50)+" ₹");
        $('#chkbutton').text("Procced to Checkout")
    }
    else{
        $('#chkbutton').text("Continue Shopping")
    }
}
function redirecttocheckout(){
    if ($('#chkbutton').text() == "Continue Shopping"){
        window.location = "index.html"
    }
    else{
        sessionStorage.setItem("subtotal",$('#subtotal').html());
        sessionStorage.setItem("shippingfee",$('#shippingfee').html());
        sessionStorage.setItem("grandtotal",$('#grandtotal').html());
        window.location = "checkout.html"
    }
}
function pluscounter(id,price,uid){
    var chprice = price
    var chqty = $("#qty"+id).val()
    $("#prototal"+id).html(parseInt(chprice)*parseInt(chqty) + " ₹")
    var matched = $(".prototal");
    var alltotal=0
    for(var i=0 ; i < matched.length ; i++){
        alltotal = parseInt(alltotal) + parseInt($("#prototal"+i).html().replace(' ₹',''))
    };
    $('#subtotal').html(alltotal+" ₹");
    $('#shippingfee').html("50 ₹");
    $('#grandtotal').html((parseInt(alltotal)+50)+" ₹");
    updCart(uid,chqty);
}
function minuscounter(id,price,uid){
    var chprice = price
    var chqty = $("#qty"+id).val()
    $("#prototal"+id).html(parseInt(chprice)*parseInt(chqty)+ " ₹")
    var matched = $(".prototal");
    var alltotal=0
    for(var i=0 ; i < matched.length ; i++){
        alltotal = parseInt(alltotal) + parseInt($("#prototal"+i).html().replace(' ₹',''))
    };
    $('#subtotal').html(alltotal+" ₹");
    $('#shippingfee').html("50 ₹");
    $('#grandtotal').html((parseInt(alltotal)+50)+" ₹");
    updCart(uid,chqty);
}
function deleteCartItem(id){
    var userId=Cookies.get('permUserId')?Cookies.get('permUserId'):Cookies.get('tempUser');
    var path = serverpath + "deletecartitems/'"+userId+"'/'"+id+"'";
    ajaxget(path,'removeCartItems','comment','control');
};
function removeCartItems(data,control){ 
    data=JSON.parse(data);
    var data1=data.recordset[0]
    if (data1.ReturnValue == "1") {
        var ct = Cookies.get('cartcount')
        ct = parseInt(ct)-1;
        Cookies.set('cartcount', ct ,{ expires: 365, path: '/' });
        $("#cartvalue").attr("data-count",Cookies.get('cartcount'));
        fetchUserCart();   
    }
};
function updCart(id,quantity){
    var MasterData = {
        "UniqueID":id,
        "UserID":'0',
        "Product_ID":'0',
        "Quantity":quantity,
        "EntryFrom":'0',
        "Price":'0',
        "DiscountedPrice":'0',
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "usercart";
    ajaxpost(path, 'updToCart', 'comment', MasterData, 'control')
}
function updToCart(data){

}