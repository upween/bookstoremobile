jQuery(document).ready(function($) {
    var path = serverpath + "rolemenu/1"
    ajaxget(path,'parsemenu','comment','control');
});
function parsemenu(data,control){  
    data = JSON.parse(data);
    sessionStorage.setItem("MenuItems",JSON.stringify(data.recordset)); 
    paintmenu();
}
if(!Cookies.get('tempUser')){
    Cookies.set('tempUser', Date.now(),{ expires: 365, path: '/' });  
}
var app = {
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },
    onDeviceReady: function() {
        sessionStorage.setItem("uuid",device.uuid)
        sessionStorage.setItem("platform",device.platform)
        sessionStorage.setItem("model",device.model)
        sessionStorage.setItem("cordova",device.cordova)
    }
};

app.initialize();